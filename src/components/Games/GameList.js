import React, { useEffect, useState } from "react";
import './GameList.css';
import axios from 'axios';
import pict from '../../assets/images/game.png';

export default function GameList() {

    return (
        <div>
            <section className="boxs" onClick={() => window.location = "/DetailGames"}>
                <div className="box">
                    <img className="logoJ" src={pict} alt="boy" />
                    <p className="textJ">Rock, Paper, Scissor</p>
                </div>
            </section>
        </div>
    )
}



