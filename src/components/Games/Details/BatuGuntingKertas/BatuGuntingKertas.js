import React, { useEffect, useState } from "react";
import './BatuGuntingKertas.css';
import foto from '../../../../assets/images/main-bg.jpg';
import axios from 'axios';
import { toast } from "react-toastify";
import { Link } from "react-router-dom";

export default function BatuGuntingKertas() {
    const token = sessionStorage.getItem("accessToken");
    const [score, setScore] = useState([]);
    const [items, setItems] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:8080/api/users').then(res => {
            setItems(res.data);
            // console.log(res.data);
            // setScore(res.data.scores);
        });
    }, []);

    function checkToken() {
        if (token !== null) {
            window.location.href = '/GamesBatuGuntingKertas';
        } else {
            toast.error("Anda harus login terlebih dahulu untuk memulai permainan.");
        }
    }

    return (
        <div className="containerBatu">
            <div className="row">
                <div className="col-xl-6 mb-5 kiri">
                    <div>
                        <h3 className="gamedetails_texth3">Rock, Paper, Scissor</h3>
                        <p className='gamedetails_textp'>
                        A classic two-person game. Players start each round by click, “rock, paper, scissors!” Rock crushes scissors, scissors cut paper, and paper covers rock. See who wins each round!
                        </p>
                    </div>
                    <div className="bungkus">
                        <p className="high_p">Highscore Top 3</p>
                        <table className='table_leaderboard table-hover table-striped table-bordered'>
                            <thead>
                                <tr className='tr'>
                                    <th className='th'>
                                        <p>Name</p>
                                    </th>
                                    <th className='th'>
                                        <p>Score</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {items.sort((a, b) => b.total_score - a.total_score).map((item, index) => (
                                    <tr key={index} className='tr' data-index={index}>
                                        <td className='td'>
                                            <Link to={'/profile'} state={ item.id }>
                                                {item.username}
                                            </Link>
                                        </td>
                                        <td className='td'>{item.total_score ?? 0 }</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="b_play d-grid  ">
                        <button className="play btn btn-primary" type="button" onClick={() => checkToken()}>Play Now</button>
                    </div>
                </div>
                <br />
                <div className="col-xl-6 mb-5 kanan">
                    <img
                        className="bkg"
                        src={foto}
                        alt="foto" />
                </div>
            </div>
        </div>
    )
}