import React, { useState } from "react";
import axios from 'axios';
import styles from './Login.css'
function Login(){
  const[password,setPassword]= useState("")
  const[email,setEmail]=useState("")
  let handleSubmit = (e) =>{
    e.preventDefault();

    if ( email === "" || password === "" ) {
       document.querySelector(".error").innerHTML = "Password or email Require";
    } else {
      // console.log({email,password});
      
      document.querySelector(".error").innerHTML = "";

      axios.post('http://localhost:8080/api/login', { email, password })
      .then( res => {
        // console.log(res.data.data.accessToken);
        
        sessionStorage.setItem('accessToken', res.data.data.accessToken);
        sessionStorage.setItem('id', res.data.data.id);
        
        const user = res.data.data.email;
        
        // console.log(res.data.data);
        
        return user;
      }).then(user=>{
        alert("Selamat Datang "+user.toUpperCase());
        window.location="/";
      })
      .catch(err=>{
        console.log(err);
        document.querySelector(".error").innerHTML=err.response.data.message;
      });
    }
  }

  return(
      <>
      <div className="background-login"></div>
      <form className="input">
          <div className="inputContainer">
          <h1>LOGIN</h1>
          <div className="inputBox">
              <input type="text"required="required" name="email"onChange={(value)=>setEmail(value.target.value)}/>
              <span>email</span>
          </div>
          <div className="inputBox">
              <input type="password"required="required" name="password"onChange={(value)=>setPassword(value.target.value)}/>
              <span>Password</span>
          </div>
          <h5 className="error"></h5>
          <button className="buttonLogin" onClick={ (e) => handleSubmit(e) }>LOGIN</button>
          <a className="login-nav"href="/Register">Sign Up</a>
          <a className="login-nav"href="/">Back</a>
          </div>
      </form>
      </>
  )
}
export default Login;