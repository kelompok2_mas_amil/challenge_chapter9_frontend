import React, { useEffect, useState } from 'react';
import NavBar from "../Navbar/Navbar";
import "./Profile.css"
import { useNavigate } from 'react-router';
import { useLocation } from 'react-router-dom';
import axios from 'axios';


const Profile = () => {
  const { state } = useLocation();
  const token = sessionStorage.getItem("accessToken");
  const id = sessionStorage.getItem("id")
  const [currentUser, setCurrentUser] = useState("");
  const [userId, setUserId] = useState("");

  useEffect(() => {
    if (state === null) {
      axios.get('http://localhost:8080/api/user/'+id, {
        headers: {
          'Authorization': token
        }
      }).then(res => {
        setCurrentUser(res.data);
        // console.log(res.data);
      });
    } else {
      axios.get('http://localhost:8080/api/user/'+state, {
        headers: {
          'Authorization': token
        }
      }).then(res => {
        setUserId(res.data.id);
        setCurrentUser(res.data);
      });
    }
  }, []);

  const navigation = useNavigate();

  const InputComponent = function() {
    let hidden;
    if (state !== null) {
      hidden = true;
    } else {
      hidden = false;
    }
    
    return (
      <button className='edit-button' hidden={hidden} onClick={() => goToUpdate()}>EDIT</button>
    );
  }

  function goToUpdate() {
    navigation("/Update")
  }

  return (
    <>
    <NavBar/>
    <div className='background'>
      <div className='profile-container'>
        <div className='user-data'>
          <div>
            <h2 className='data-title'>USER ID</h2>
            <p>{currentUser.id ? currentUser.id : "Loading.."}</p>
          </div>
          <div>
            <h2 className='data-title'>USERNAME</h2>
            <p>{currentUser.username ? currentUser.username : "Loading.."}</p>
          </div>
          <div>
            <h2 className='data-title'>EMAIL</h2>
            <p>{currentUser.email ? currentUser.email : "Loading.."}</p>
          </div>
          <div>
            <h2 className='data-title'>BIO</h2>
            <p>{currentUser.bio ? currentUser.bio : "Loading.."}</p>
          </div>
          <div>
            <h2 className='data-title'>CITY</h2>
            <p>{currentUser.city ? currentUser.city : "Loading.."}</p>
          </div>
          <div>
            <h2 className='data-title'>TOTAL SCORE</h2>
            <p>{currentUser.total_score ? currentUser.total_score : "0"}</p>
          </div>
          <InputComponent />
        </div>
      </div>
    </div>
    </>
  )
}

export default Profile